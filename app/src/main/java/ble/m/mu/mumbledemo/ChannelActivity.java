package ble.m.mu.mumbledemo;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.morlunk.jumble.Constants;
import com.morlunk.jumble.JumbleService;
import com.morlunk.jumble.model.Server;
import com.morlunk.jumble.util.JumbleException;
import com.morlunk.jumble.util.JumbleObserver;
import com.morlunk.jumble.util.ParcelableByteArray;

import org.spongycastle.util.encoders.Hex;

import java.io.ByteArrayInputStream;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.security.cert.CertificateException;

import static android.media.MediaRecorder.*;

public class ChannelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel);

        View view = findViewById(R.id.btnConnect);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectToServer(new Server(0, "jugregator.org", "jugregator.org", 64738, "test_jumble", ""));
            }
        });

        View v2 = findViewById(R.id.btnDisconnect);
        v2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mService!= null){
                    try {
                        mService.disconnect();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private JumbleService.JumbleBinder mService;
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = (JumbleService.JumbleBinder) service;
            try {
                mService.registerObserver(mObserver);
                //mService.clearChatNotifications(); // Clear chat notifications on resume.
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            //mDrawerAdapter.notifyDataSetChanged();

            /*
            for(JumbleServiceFragment fragment : mServiceFragments)
                fragment.setServiceBound(true);
            */

            // Re-show server list if we're showing a fragment that depends on the service.
            /*
            try {

                if(getSupportFragmentManager().findFragmentById(R.id.content_frame) instanceof JumbleServiceFragment &&
                        mService.getConnectionState() != JumbleService.STATE_CONNECTED) {
                    loadDrawerFragment(DrawerAdapter.ITEM_FAVOURITES);
                }
                updateConnectionState(getService());

            } catch (RemoteException e) {
                e.printStackTrace();
            }
                          */
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

    private JumbleObserver mObserver = new JumbleObserver() {
        @Override
        public void onConnected() throws RemoteException {
            //loadDrawerFragment(DrawerAdapter.ITEM_SERVER);
            //mDrawerAdapter.notifyDataSetChanged();
            supportInvalidateOptionsMenu();

            //updateConnectionState(getService());
        }

        @Override
        public void onConnecting() throws RemoteException {
            //updateConnectionState(getService());
        }

        @Override
        public void onDisconnected(JumbleException e) throws RemoteException {
            // Re-show server list if we're showing a fragment that depends on the service.
            /*
            if(getSupportFragmentManager().findFragmentById(R.id.content_frame) instanceof JumbleServiceFragment) {
                loadDrawerFragment(DrawerAdapter.ITEM_FAVOURITES);
            }
            mDrawerAdapter.notifyDataSetChanged();
            supportInvalidateOptionsMenu();

            updateConnectionState(getService());
            */
        }

        @Override
        public void onTLSHandshakeFailed(ParcelableByteArray cert) throws RemoteException {
            byte[] certBytes = cert.getBytes();
            final Server lastServer = mService.getConnectedServer();

            try {
                CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
                final X509Certificate x509 = (X509Certificate) certFactory.generateCertificate(new ByteArrayInputStream(certBytes));

                AlertDialog.Builder adb = new AlertDialog.Builder(ChannelActivity.this);
                adb.setTitle("Untrusted cert");
                try {
                    MessageDigest digest = MessageDigest.getInstance("SHA-1");
                    byte[] certDigest = digest.digest(x509.getEncoded());
                    String hexDigest = new String(Hex.encode(certDigest));
                    adb.setMessage("Cert info" +
                    //        x509.getSubjectDN().getName(),
                    //        x509.getNotBefore().toString(),
                    //        x509.getNotAfter().toString(),
                            hexDigest);

                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                    adb.setMessage(x509.toString());
                }
                adb.setPositiveButton("ALLOW", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Try to add to trust store
                        try {
                            String alias = lastServer.getHost();
                            KeyStore trustStore = PlumbleTrustStore.getTrustStore(ChannelActivity.this);
                            trustStore.setCertificateEntry(alias, x509);
                            PlumbleTrustStore.saveTrustStore(ChannelActivity.this, trustStore);
                            Toast.makeText(ChannelActivity.this, "ADDED", Toast.LENGTH_LONG).show();
                            connectToServer(lastServer);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ChannelActivity.this, "FAILED", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                adb.setNegativeButton("Cancel", null);
                adb.show();
            } catch (java.security.cert.CertificateException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onPermissionDenied(String reason) throws RemoteException {
            AlertDialog.Builder adb = new AlertDialog.Builder(ChannelActivity.this);
            adb.setTitle(R.string.perm_denied);
            adb.setMessage(reason);
            adb.show();
        }
    };



    private static class ConnectTask extends AsyncTask<Server, Void, Intent>{

        private final Context mContext;
        private final Settings mSettings;

        public ConnectTask(Context ctx){
            mContext = ctx;
            mSettings = Settings.getInstance(ctx);
        }

        @Override
        protected Intent doInBackground(Server... params) {
            Server server = params[0];

        /* Convert input method defined in settings to an integer format used by Jumble. */
            int inputMethod = Constants.TRANSMIT_PUSH_TO_TALK;

            int audioSource = MediaRecorder.AudioSource.MIC;
            int audioStream =  AudioManager.STREAM_MUSIC;

            String applicationVersion = "";
            try {
                applicationVersion = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            Intent connectIntent = new Intent(mContext, JumbleService.class);
            connectIntent.putExtra(JumbleService.EXTRAS_SERVER, server);
            connectIntent.putExtra(JumbleService.EXTRAS_CLIENT_NAME, mContext.getString(R.string.app_name)+" "+applicationVersion);
            connectIntent.putExtra(JumbleService.EXTRAS_TRANSMIT_MODE, inputMethod);
            connectIntent.putExtra(JumbleService.EXTRAS_DETECTION_THRESHOLD, mSettings.getDetectionThreshold());
            connectIntent.putExtra(JumbleService.EXTRAS_AMPLITUDE_BOOST, mSettings.getAmplitudeBoostMultiplier());
            connectIntent.putExtra(JumbleService.EXTRAS_CERTIFICATE, mSettings.getCertificate());
            connectIntent.putExtra(JumbleService.EXTRAS_CERTIFICATE_PASSWORD, mSettings.getCertificatePassword());
            connectIntent.putExtra(JumbleService.EXTRAS_AUTO_RECONNECT, mSettings.isAutoReconnectEnabled());
            connectIntent.putExtra(JumbleService.EXTRAS_AUTO_RECONNECT_DELAY, 60);
            connectIntent.putExtra(JumbleService.EXTRAS_USE_OPUS, !mSettings.isOpusDisabled());
            connectIntent.putExtra(JumbleService.EXTRAS_INPUT_RATE, mSettings.getInputSampleRate());
            connectIntent.putExtra(JumbleService.EXTRAS_INPUT_QUALITY, mSettings.getInputQuality());
            connectIntent.putExtra(JumbleService.EXTRAS_FORCE_TCP, mSettings.isTcpForced());
            connectIntent.putExtra(JumbleService.EXTRAS_USE_TOR, mSettings.isTorEnabled());
            connectIntent.putStringArrayListExtra(JumbleService.EXTRAS_ACCESS_TOKENS, new ArrayList<String>());
            connectIntent.putExtra(JumbleService.EXTRAS_AUDIO_SOURCE, audioSource);
            connectIntent.putExtra(JumbleService.EXTRAS_AUDIO_STREAM, audioStream);
            connectIntent.putExtra(JumbleService.EXTRAS_FRAMES_PER_PACKET, mSettings.getFramesPerPacket());
            connectIntent.putExtra(JumbleService.EXTRAS_TRUST_STORE, PlumbleTrustStore.getTrustStorePath(mContext));
            connectIntent.putExtra(JumbleService.EXTRAS_TRUST_STORE_PASSWORD, PlumbleTrustStore.getTrustStorePassword());
            connectIntent.putExtra(JumbleService.EXTRAS_TRUST_STORE_FORMAT, PlumbleTrustStore.getTrustStoreFormat());
            connectIntent.putExtra(JumbleService.EXTRAS_HALF_DUPLEX, mSettings.isHalfDuplex());
            connectIntent.putExtra(JumbleService.EXTRAS_ENABLE_PREPROCESSOR, mSettings.isPreprocessorEnabled());
            /*
            if (server.isSaved()) {
                ArrayList<Integer> muteHistory = (ArrayList<Integer>) mDatabase.getLocalMutedUsers(server.getId());
                ArrayList<Integer> ignoreHistory = (ArrayList<Integer>) mDatabase.getLocalIgnoredUsers(server.getId());
                connectIntent.putExtra(JumbleService.EXTRAS_LOCAL_MUTE_HISTORY, muteHistory);
                connectIntent.putExtra(JumbleService.EXTRAS_LOCAL_IGNORE_HISTORY, ignoreHistory);
            }
            */
            connectIntent.setAction(JumbleService.ACTION_CONNECT);
            return connectIntent;
        }

        @Override
        protected void onPostExecute(Intent intent) {
            super.onPostExecute(intent);
            mContext.startService(intent);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Intent connectIntent = new Intent(this, JumbleService.class);
        bindService(connectIntent, mConnection, 0);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(mService != null)
            try {
                /*
                for(JumbleServiceFragment fragment : mServiceFragments)
                    fragment.setServiceBound(false);
                */
                mService.unregisterObserver(mObserver);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        unbindService(mConnection);
    }


    public void connectToServer(final Server server) {
        // Check if we're already connected to a server; if so, inform user.
        try {
            if(mService != null &&
                    mService.getConnectionState() == JumbleService.STATE_CONNECTED) {
                AlertDialog.Builder adb = new AlertDialog.Builder(this);
                adb.setMessage("RECONNECT");
                adb.setPositiveButton("CONNECT", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            // Register an observer to reconnect to the new server once disconnected.
                            mService.registerObserver(new JumbleObserver() {
                                @Override
                                public void onDisconnected(JumbleException e) throws RemoteException {
                                    connectToServer(server);
                                    mService.unregisterObserver(this);
                                }
                            });
                            mService.disconnect();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                });
                adb.setNegativeButton(android.R.string.cancel, null);
                adb.show();
                return;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        // Prompt to start Orbot if enabled but not running

        ConnectTask connectTask = new ConnectTask(this);
        connectTask.execute(server);
    }
}
